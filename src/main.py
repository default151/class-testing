'''This is some trivial code.
'''
old_print = print


def print(x):
    old_print(x)


if __name__ == "__main__":
    print("Hello, world!")
